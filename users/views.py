from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm, forms


class myUserCreationForm(UserCreationForm):
    username = forms.CharField(
        label='Nombre de usuario',
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    password1 = forms.CharField(
        label='Contraseña',
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )
    password2 = forms.CharField(
        label='Confirmar contraseña',
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )


def register(request):
    form = myUserCreationForm()
    return render(request, 'users/register.html', {'form': form})
