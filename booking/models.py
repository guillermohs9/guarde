from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from datetime import date


class Dueño(models.Model):
    nombre = models.CharField(max_length=100)
    telefono_1 = models.CharField(max_length=100, default=None, blank=True, null=True)
    telefono_2 = models.CharField(max_length=100, default=None, blank=True, null=True)
    telefono_3 = models.CharField(max_length=100, default=None, blank=True, null=True)
    email = models.CharField(max_length=150, default=None, blank=True, null=True)
    domicilio = models.TextField(default=None, blank=True, null=True)
    observaciones = models.TextField(default=None, blank=True, null=True)
    usuario_carga = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha_carga = models.DateTimeField(default=timezone.now)
    # mascota_set


class TipoMascota(models.Model):
    descripcion = models.CharField(max_length=100)


class Mascota(models.Model):
    nombre = models.CharField(max_length=100)
    foto = models.TextField(default=None, blank=True, null=True)
    fecha_nac = models.DateField(default=None, blank=True, null=True)
    tipo = models.ForeignKey(TipoMascota, on_delete=models.CASCADE)
    raza = models.CharField(max_length=100, default=None, blank=True, null=True)
    descripcion = models.TextField(default=None, blank=True, null=True)
    marca_alimento = models.CharField(max_length=100, default=None, blank=True, null=True)
    indicaciones = models.TextField(default=None, blank=True, null=True)
    foto_libreta = models.TextField(default=None, blank=True, null=True)
    dueño = models.ForeignKey(Dueño, on_delete=models.CASCADE, default=None)
    # al agregar una mascota, en el formulario dar la opcion en frontend:
    # el dueño ya existe? sino cargar los datos ahi mismo y se agregan
    # las 2 cosas juntas. Si existe, elegir de la lista
    usuario_carga = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha_carga = models.DateTimeField(default=timezone.now)
    # reserva_set
    
    def edad(self):
        today = timezone.now().date()
        return today.year - self.fecha_nac.year - ((today.month, today.day) < (self.fecha_nac.month, self.fecha_nac.day))

    
class Reserva(models.Model):
    fecha_in = models.DateField()
    fecha_out = models.DateField()
    mascota = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    # Se debe poder agregar mas de una mascota por reserva. Si hay
    # mas de una mascota, crear reservas para cada mascota. Las
    # reservas son individuales.
    baño = models.BooleanField(default=False)
    correa = models.BooleanField(default=True)
    alimento = models.BooleanField(default=True)
    libreta = models.BooleanField(default=True)
    indicaciones = models.TextField(default=None, blank=True, null=True)
    observaciones = models.TextField(default=None, blank=True, null=True)
    usuario_carga = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha_carga = models.DateTimeField(default=timezone.now)
    
    def ingresa_hoy(self):
        return self.fecha_in == timezone.now().date()
        
    def duracion(self):
        return (self.fecha_out - self.fecha_in).days + 1
    
    def sale_hoy(self):
        return self.fecha_out == timezone.now().date()
    
    