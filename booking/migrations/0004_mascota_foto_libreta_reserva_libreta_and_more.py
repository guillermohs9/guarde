# Generated by Django 4.0.1 on 2022-01-11 17:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0003_rename_observaciones_mascota_descripcion'),
    ]

    operations = [
        migrations.AddField(
            model_name='mascota',
            name='foto_libreta',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='reserva',
            name='libreta',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='reserva',
            name='baño',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='reserva',
            name='correa',
            field=models.BooleanField(default=True),
        ),
    ]
