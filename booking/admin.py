from django.contrib import admin
from .models import Dueño, Mascota, Reserva, TipoMascota

admin.site.register(Dueño)
admin.site.register(Mascota)
admin.site.register(Reserva)
admin.site.register(TipoMascota)