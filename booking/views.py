from django.shortcuts import render
from django.http import JsonResponse
from .models import Reserva
from django.utils import timezone


def home(request):
    context = {
        'reservas': Reserva.objects.filter(fecha_in__lte=timezone.now(), \
                                           fecha_out__gte=timezone.now()).all(),
        'ingresos': Reserva.objects.filter(fecha_in=timezone.now()).all(),
        'salidas': Reserva.objects.filter(fecha_out=timezone.now()).all()
    }
    return render(request, 'booking/home.html', context)


def about(request):
    return render(request, 'booking/about.html', {'title': 'About'})


def calendar(request):
    return render(request, 'booking/calendar.html', {'title': 'Calendario'})

